import React, { Component } from "react";
import Leyenda1 from "../img/hu-sb/leyenda.PNG";
import Img11 from "../img/hu-sb/1.PNG";
import Img12 from "../img/hu-sb/2.PNG";
import Img13 from "../img/hu-sb/3.PNG";
import Img14 from "../img/hu-sb/4.PNG";
import Img15 from "../img/hu-sb/5.PNG";

import Leyenda2 from "../img/hu-bbvssbmr/leyenda.PNG";
import Img21 from "../img/hu-bbvssbmr/1.PNG";
import Img22 from "../img/hu-bbvssbmr/2.PNG";
import Img23 from "../img/hu-bbvssbmr/3.PNG";
import Img24 from "../img/hu-bbvssbmr/4.PNG";
import Img25 from "../img/hu-bbvssbmr/5.PNG";

import Leyenda3 from "../img/hu-bbvssblimp/leyenda.PNG";
import Img31 from "../img/hu-bbvssblimp/1.PNG";
import Img32 from "../img/hu-bbvssblimp/2.PNG";
import Img33 from "../img/hu-bbvssblimp/3.PNG";
import Img34 from "../img/hu-bbvssblimp/4.PNG";
import Img35 from "../img/hu-bbvssblimp/5.PNG";

import Leyenda4 from "../img/3h-bu/leyenda.PNG";
import Img41 from "../img/3h-bu/1.PNG";
import Img42 from "../img/3h-bu/2.PNG";
import Img43 from "../img/3h-bu/3.PNG";
import Img44 from "../img/3h-bu/4.PNG";
import Img45 from "../img/3h-bu/5.PNG";

import Leyenda5 from "../img/3h-sb/leyenda.PNG";
import Img51 from "../img/3h-sb/1.PNG";
import Img52 from "../img/3h-sb/2.PNG";
import Img53 from "../img/3h-sb/3.PNG";
import Img54 from "../img/3h-sb/4.PNG";
import Img55 from "../img/3h-sb/5.PNG";

import Leyenda6 from "../img/3h-sbvsbumr/leyenda.PNG";
import Img61 from "../img/3h-sbvsbumr/1.PNG";
import Img62 from "../img/3h-sbvsbumr/2.PNG";
import Img63 from "../img/3h-sbvsbumr/3.PNG";
import Img64 from "../img/3h-sbvsbumr/4.PNG";
import Img65 from "../img/3h-sbvsbumr/5.PNG";

import Leyenda7 from "../img/3h-sbvsbulimp/leyenda.PNG";
import Img71 from "../img/3h-sbvsbulimp/1.PNG";
import Img72 from "../img/3h-sbvsbulimp/2.PNG";
import Img73 from "../img/3h-sbvsbulimp/3.PNG";
import Img74 from "../img/3h-sbvsbulimp/4.PNG";
import Img75 from "../img/3h-sbvsbulimp/5.PNG";

import Leyenda8 from "../img/3h-bbvsbumr/leyenda.PNG";
import Img81 from "../img/3h-bbvsbumr/1.PNG";
import Img82 from "../img/3h-bbvsbumr/2.PNG";
import Img83 from "../img/3h-bbvsbumr/3.PNG";
import Img84 from "../img/3h-bbvsbumr/4.PNG";
import Img85 from "../img/3h-bbvsbumr/5.PNG";

import Leyenda9 from "../img/3h-bbvsbumr-sbcall/leyenda.PNG";
import Img91 from "../img/3h-bbvsbumr-sbcall/1.PNG";
import Img92 from "../img/3h-bbvsbumr-sbcall/2.PNG";
import Img93 from "../img/3h-bbvsbumr-sbcall/3.PNG";
import Img94 from "../img/3h-bbvsbumr-sbcall/4.PNG";
import Img95 from "../img/3h-bbvsbumr-sbcall/5.PNG";

import Leyenda10 from "../img/3h-bbvsbulimp/leyenda.PNG";
import Img101 from "../img/3h-bbvsbulimp/1.PNG";
import Img102 from "../img/3h-bbvsbulimp/2.PNG";
import Img103 from "../img/3h-bbvsbulimp/3.PNG";
import Img104 from "../img/3h-bbvsbulimp/4.PNG";
import Img105 from "../img/3h-bbvsbulimp/5.PNG";

import Leyenda11 from "../img/3h-bbvssbmr/leyenda.PNG";
import Img111 from "../img/3h-bbvssbmr/1.PNG";
import Img112 from "../img/3h-bbvssbmr/2.PNG";
import Img113 from "../img/3h-bbvssbmr/3.PNG";
import Img114 from "../img/3h-bbvssbmr/4.PNG";
import Img115 from "../img/3h-bbvssbmr/5.PNG";

import Leyenda12 from "../img/3h-bbvssblimp/leyenda.PNG";
import Img121 from "../img/3h-bbvssblimp/1.PNG";
import Img122 from "../img/3h-bbvssblimp/2.PNG";
import Img123 from "../img/3h-bbvssblimp/3.PNG";
import Img124 from "../img/3h-bbvssblimp/4.PNG";
import Img125 from "../img/3h-bbvssblimp/5.PNG";

import Img131 from "../img/nash/1.PNG";
import Img132 from "../img/nash/2.PNG";
import Img133 from "../img/nash/3.PNG";
import Img134 from "../img/nash/4.PNG";
import Img135 from "../img/nash/5.PNG";

export default class leyenda extends Component {
  render() {
    return (
      <div className="body">
        <div className="row">
          <p style={{ width: "230px" }}>[HU] SB</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda1);
              this.props.setRango(Img11);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda1);
              this.props.setRango(Img12);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda1);
              this.props.setRango(Img13);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda1);
              this.props.setRango(Img14);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda1);
              this.props.setRango(Img15);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[HU] BB vs SB MR</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda2);
              this.props.setRango(Img21);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda2);
              this.props.setRango(Img22);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda2);
              this.props.setRango(Img23);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda2);
              this.props.setRango(Img24);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda2);
              this.props.setRango(Img25);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[HU] BB vs SB limp</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda3);
              this.props.setRango(Img31);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda3);
              this.props.setRango(Img32);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda3);
              this.props.setRango(Img33);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda3);
              this.props.setRango(Img34);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda3);
              this.props.setRango(Img35);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <br />
        <div className="row">
          <p style={{ width: "230px" }}>[3H] BU</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda4);
              this.props.setRango(Img41);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda4);
              this.props.setRango(Img42);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda4);
              this.props.setRango(Img43);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda4);
              this.props.setRango(Img44);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda4);
              this.props.setRango(Img45);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <br />
        <div className="row">
          <p style={{ width: "230px" }}>[3H] SB</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda5);
              this.props.setRango(Img51);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda5);
              this.props.setRango(Img52);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda5);
              this.props.setRango(Img53);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda5);
              this.props.setRango(Img54);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda5);
              this.props.setRango(Img55);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[3H] SB vs BU MR</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda6);
              this.props.setRango(Img61);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda6);
              this.props.setRango(Img62);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda6);
              this.props.setRango(Img63);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda6);
              this.props.setRango(Img64);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda6);
              this.props.setRango(Img65);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[3H] SB vs BU limp</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda7);
              this.props.setRango(Img71);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda7);
              this.props.setRango(Img72);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda7);
              this.props.setRango(Img73);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda7);
              this.props.setRango(Img74);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda7);
              this.props.setRango(Img75);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <br />
        <div className="row">
          <p style={{ width: "230px" }}>[3H] BB vs BU MR</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda8);
              this.props.setRango(Img81);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda8);
              this.props.setRango(Img82);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda8);
              this.props.setRango(Img83);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda8);
              this.props.setRango(Img84);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda8);
              this.props.setRango(Img85);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[3H] BB vs BU MR + SB CALL</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda9);
              this.props.setRango(Img91);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda9);
              this.props.setRango(Img92);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda9);
              this.props.setRango(Img93);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda9);
              this.props.setRango(Img94);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda9);
              this.props.setRango(Img95);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[3H] BB vs BU limp</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda10);
              this.props.setRango(Img101);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda10);
              this.props.setRango(Img102);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda10);
              this.props.setRango(Img103);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda10);
              this.props.setRango(Img104);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda10);
              this.props.setRango(Img105);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[3H] BB vs SB MR</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda11);
              this.props.setRango(Img111);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda11);
              this.props.setRango(Img112);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda11);
              this.props.setRango(Img113);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda11);
              this.props.setRango(Img114);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda11);
              this.props.setRango(Img115);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[3H] BB vs SB limp</p>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda12);
              this.props.setRango(Img121);
            }}
          >
            22+
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda12);
              this.props.setRango(Img122);
            }}
          >
            17.5 - 21.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda12);
              this.props.setRango(Img123);
            }}
          >
            13 - 17
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda12);
              this.props.setRango(Img124);
            }}
          >
            9 - 12.5
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda(Leyenda12);
              this.props.setRango(Img125);
            }}
          >
            6 - 8.5
          </button>
        </div>
        <br />
        <div className="row">
          <p style={{ width: "230px" }}>[NASH]</p>
          <button
            onClick={() => {
              this.props.setLeyenda("");
              this.props.setRango(Img131);
            }}
          >
            HU - BB
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda("");
              this.props.setRango(Img132);
            }}
          >
            HU - SB vs BU
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda("");
              this.props.setRango(Img133);
            }}
          >
            3H - BB vs SB
          </button>
        </div>
        <div className="row">
          <p style={{ width: "230px" }}>[NASH]</p>
          <button
            onClick={() => {
              this.props.setLeyenda("");
              this.props.setRango(Img134);
            }}
          >
            3H - BB vs BU
          </button>
          <button
            onClick={() => {
              this.props.setLeyenda("");
              this.props.setRango(Img135);
            }}
          >
            3H - BB vs BU + SB
          </button>
        </div>
      </div>
    );
  }
}
