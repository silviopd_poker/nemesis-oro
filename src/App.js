import React, { useState } from "react";
import Rango from "./components/rango";
import Leyenda from "./components/leyenda";
import Body from "./components/body";
import "./App.css";

function App() {
  const [rango, setRango] = useState("");
  const [leyenda, setLeyenda] = useState("");

  return (
    <div className="App">
      <div className="row" style={{ marginTop: "20px" }}>
        <div style={{ width: "550px", marginLeft: "30px" }}>
          <Body setRango={setRango} setLeyenda={setLeyenda} />
        </div>
        <div className="col-3">
          <Rango rango={rango} />
          <Leyenda leyenda={leyenda} />
        </div>
      </div>
    </div>
  );
}

export default App;
